package domain

type Result[A] = Either[DomainError, A]

enum DomainError:
  case IOFileProblem(error: String)
  case XMLError(error: String)
  case InvalidHumanId(error: String)
  case InvalidOrderId(error: String)
  case ProductDoesNotExist(error: String)
  case InvalidPhysicalId(error: String)
  case InvalidProductId(error: String)
  case InvalidQuantity(error: String)
  case TaskDoesNotExist(error: String)
  case TaskUsesNonExistentPRT(error: String)
  case InvalidTaskId(error: String)
  case InvalidTime(error: String)
  case ResourceUnavailable(taskId: String, resourceType: String)
  case EmptyString
  case NegativeInteger
  case InvalidPhysicalResourceType(error: String)
  case ImpossibleSchedule(error: String)