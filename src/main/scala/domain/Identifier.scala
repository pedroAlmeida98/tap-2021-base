package domain

import domain.Result

trait Identifier[A]:
  def from(t: String): Result[A]

