package domain

import scala.xml.Elem
import xml.XML.*
import domain.Result
import domain.order.{Order}
import domain.product.{Product}
import domain.resource.physical.PhysicalResource
import domain.resource.human.HumanResource
import domain.task.Task

final case class Production(listTasks: List[Task], listOrders: List[Order], listProducts: List[Product], listHumans: List[HumanResource], listPhysicals: List[PhysicalResource])

object Production:
  private val PhysicalXmlNode = "Physical"
  private val HumanXmlNode = "Human"
  private val TaskXmlNode = "Task"
  private val ProductXmlNode = "Product"
  private val OrderXmlNode = "Order"

  def from(xml: Elem): Result[Production] =
    for
      listPhysicals <- traverse((xml \\ PhysicalXmlNode), PhysicalResource.from)
      listHumans <- traverse((xml \\ HumanXmlNode), HumanResource.from(listPhysicals))
      listTasks <- traverse((xml \\ TaskXmlNode), Task.from(listPhysicals))
      listProducts <- traverse((xml \\ ProductXmlNode), Product.from(listTasks))
      listOrders <- traverse((xml \\ OrderXmlNode), Order.from(listProducts))
    yield Production(listTasks, listOrders, listProducts, listHumans, listPhysicals)
