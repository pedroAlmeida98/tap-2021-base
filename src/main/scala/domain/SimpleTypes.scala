package domain

import domain.DomainError.*
import domain.Result
import scala.util.Try

object SimpleTypes:

  opaque type NonEmptyString = String

  object NonEmptyString:
    def from(s: String): Result[NonEmptyString] =
      if (s.isEmpty) then Left(EmptyString) else Right(s)

  opaque type NonNegativeInteger = Int

  object NonNegativeInteger:
    def from(int: Int): Result[NonNegativeInteger] =
      if (int < 0) Left(NegativeInteger) else Right(int)

    def to(int: NonNegativeInteger): Int = int
    
    def zero(): NonNegativeInteger = 0
  
  extension (x: NonNegativeInteger)
    infix def +(xs: NonNegativeInteger): NonNegativeInteger = x + xs

    infix def <(xs: NonNegativeInteger): Boolean = x < xs
  
    infix def <=(xs: NonNegativeInteger): Boolean = x <= xs
  
    infix def >(xs: NonNegativeInteger): Boolean = x > xs
  
    infix def >=(xs: NonNegativeInteger): Boolean = x >= xs