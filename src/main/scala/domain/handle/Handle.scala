package domain.handle

import scala.xml.Node
import xml.XML.*
import domain.Result
import domain.resource.physical.*
import domain.resource.human.HumanResourceId
import domain.DomainError.{TaskUsesNonExistentPRT}

import scala.annotation.tailrec

final case class Handle(pr: PhysicalResource)

object Handle:
  private val PhysicalResourceTypeAttribute = "type"

  def from(listPhysicalResources: List[PhysicalResource])(xml: Node): Result[Handle] =
    for
      sType <- fromAttribute(xml, PhysicalResourceTypeAttribute)
      pr <- listPhysicalResources.find(t => t.prType sameAsString sType).fold(Left(TaskUsesNonExistentPRT(sType)))(ph => Right(ph))
    yield Handle(pr)

  def toPhysical(handle: Handle): PhysicalResource = handle.pr
