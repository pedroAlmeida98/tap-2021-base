package domain.order

import scala.xml.Node
import domain.Result
import domain.product.*
import domain.DomainError.*
import domain.order.{OrderId, Quantity}

import xml.XML.*
import domain.Identifier

final case class Order(id: OrderId, prod: Product, quantity: Quantity)

object Order:
  private val OrderIdAttribute = "id"
  private val ProductReferenceAttribute = "prdref"
  private val QuantityAttribute = "quantity"

  def from(products: List[Product])(xml: Node): Result[Order] =
    for
      sOrderid <- fromAttribute(xml, OrderIdAttribute)
      orderId <- OrderId.from(sOrderid)
      sproductdRef <- fromAttribute(xml, ProductReferenceAttribute)
      productId <- ProductId.from(sproductdRef)
      product <- validateProductId(products, productId)
      sQuantity <- fromAttribute(xml, QuantityAttribute)
      quantity <- Quantity.from(sQuantity)
    yield Order(orderId, product, quantity)

  /**
   * Checks if a product identifier is a valid one.
   *
   * @param products  list of products to compare.
   * @param productId identifier to check validity
   * @return
   */
  private def validateProductId(products: List[Product], productId: ProductId): Result[Product] =
    def auxvalidateProductId(listProd: List[Product]): Result[Product] =
      listProd match
        case Nil => Left(ProductDoesNotExist(productId.toString))
        case h :: t => if (h.id same productId) Right(h) else auxvalidateProductId(t)

    auxvalidateProductId(products)