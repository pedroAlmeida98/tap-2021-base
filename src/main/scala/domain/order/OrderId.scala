package domain.order

import domain.Result
import domain.DomainError.InvalidOrderId
import domain.Identifier

opaque type OrderId = String

object OrderId extends Identifier[OrderId] :
  def from(s: String): Result[OrderId] =
    val regex = "(ORD_)[0-9]+".r
    if (regex.matches(s)) then Right(s) else Left(InvalidOrderId(s))

extension (x: OrderId)
  infix def sameOrderId(y: OrderId): Boolean = x == y