package domain.order

import domain.Result
import domain.DomainError.InvalidQuantity
import scala.util.Try

opaque type Quantity = Int

object  Quantity:
  def from(i: Int): Result[Quantity] =
    if (i <= 0) then Left(InvalidQuantity(i.toString)) else Right(i)

  def from(s: String): Result[Quantity] =
    Try(s.toInt).fold[Result[Quantity]](_ => Left(InvalidQuantity(s)), d => from(d))

  def to(q: Quantity): Int = q

  def zero(): Quantity = 0

extension (x: Quantity)
  infix def equalsTo(y: Int): Boolean = x == y

  infix def minus(xs: Int): Quantity = x - xs

  infix def plusQuantity(xs: Int): Quantity = x + xs

  infix def +(xs: Int): Quantity = x + xs

  infix def >=(xs: Quantity): Boolean = x >= xs

  infix def >(xs: Quantity): Boolean = x > xs
