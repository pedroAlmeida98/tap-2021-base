package domain.product

import domain.Result
import domain.SimpleTypes.NonEmptyString

opaque type FullName = NonEmptyString

object FullName:
  def from(s: String): Result[FullName] = NonEmptyString.from(s)
