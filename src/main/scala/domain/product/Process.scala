package domain.product

import scala.xml.Node
import domain.task.*
import domain.Result
import xml.XML.*
import domain.DomainError.TaskDoesNotExist

final case class Process(task: Task)

object Process:
  private val TaskRefXmlAttribute = "tskref"

  def from(listTasks: List[Task])(xml: Node): Result[Process] =
    for
      taskRef <- fromAttribute(xml, TaskRefXmlAttribute)
      taskId <- TaskId.from(taskRef)
      task <- validateTaskId(listTasks, taskId)
    yield Process(task)

  /**
   * Checks if the task identifier of the process is a valid task identifier.
   *
   * @param tasks  list of valid tasks.
   * @param taskId identifier to check validity
   * @return {@see Result} of {@see Task}
   */
  private def validateTaskId(tasks: List[Task], taskId: TaskId): Result[Task] =
    def auxvalidateTaskId(listTask: List[Task]): Result[Task] =
      listTask match
        case Nil => Left(TaskDoesNotExist(taskId.toString))
        case h :: t => if (h.id sameTaskId taskId) Right(h) else auxvalidateTaskId(t)

    auxvalidateTaskId(tasks)
