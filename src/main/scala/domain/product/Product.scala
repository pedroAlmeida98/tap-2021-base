package domain.product

import scala.xml.Node
import domain.Result
import domain.product.{FullName, ProductId, Process}
import domain.DomainError.*
import scala.xml.Elem
import xml.XML.*
import domain.task.*

final case class Product(id: ProductId, fullName: FullName, tasks: List[Task])

object Product:
  private val TaskIdentifierXmlAttribute = "id"
  private val NameXmlAttribute = "name"
  private val ProcessXmlNode = "Process"
  private val TaskReferenceAttribute = "tskref"

  def from(listTasks: List[Task])(xml: Node): Result[Product] =
    for
      sProductid        <- fromAttribute(xml, "id")
      productId         <- ProductId.from(sProductid)
      sFullName         <- fromAttribute(xml, "name")
      fullName          <- FullName.from(sFullName)
      listTask  <- traverse( (xml \\ "Process"),  createTask(listTasks))
    yield Product(productId, fullName, listTask)

  /**
   * Creates a task from a xml Node.
   *
   * @param listTasks list of task to verify validity of the task.
   * @param xml       xml to convert to the task object.
   * @return newly created object.
   */
  private def createTask(listTasks: List[Task])(xml: Node): Result[Task] =
    for
      taskRef <- fromAttribute(xml, TaskReferenceAttribute)
      taskId <- TaskId.from(taskRef)
      task <- listTasks.find(t => t.id == taskId).fold(Left(TaskDoesNotExist(taskRef)))(task => Right(task))
    yield Task(task.id, task.time, task.physicalResourceTypes)