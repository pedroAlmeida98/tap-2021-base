package domain.product

import domain.Result
import domain.DomainError.*
import domain.Identifier

opaque type ProductId = String

object ProductId extends Identifier[ProductId] :
  def from(s: String): Result[ProductId] =
    val regex = "(PRD_)[0-9]+".r
    if (regex.matches(s)) then Right(s) else Left(InvalidProductId(s))
extension (x: ProductId)
  infix def same(y: ProductId): Boolean = x == y