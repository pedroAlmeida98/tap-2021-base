package domain.resource.human
import scala.xml.Node
import xml.XML.*
import domain.Result
import domain.resource.physical.*
import domain.resource.human.{HumanResourceId}
import domain.handle.Handle

final case class HumanResource(id: HumanResourceId, name: String, physicalResourceTypes: List[PhysicalResourceType])

object HumanResource:
  private val HumanResourceIdentifierXmlAttribute = "id"
  private val NameXmlAttribute = "name"
  private val HandlesXmlNode = "Handles"

  def from(listPhysicalResources: List[PhysicalResource])(xml: Node): Result[HumanResource] =
    for
      sId <- fromAttribute(xml, HumanResourceIdentifierXmlAttribute)
      id <- HumanResourceId.from(sId)
      name <- fromAttribute(xml, NameXmlAttribute)
      handles <- traverse((xml \\ HandlesXmlNode), Handle.from(listPhysicalResources))
    yield HumanResource(id, name, handles.map(h => h.pr.prType))