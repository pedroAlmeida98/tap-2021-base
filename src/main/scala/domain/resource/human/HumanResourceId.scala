package domain.resource.human

import domain.Result
import domain.DomainError.InvalidHumanId
import domain.Identifier

opaque type HumanResourceId = String

object HumanResourceId extends Identifier[HumanResourceId] :
  def from(s: String): Result[HumanResourceId] =
    val regex = "(HRS_)[0-9]+".r
    if (regex.matches(s)) then Right(s) else Left(InvalidHumanId(s))
