package domain.resource.physical

import scala.xml.{Elem, Node}
import xml.XML.*
import domain.Result
import domain.resource.physical.{PhysicalResourceId, PhysicalResourceType}

final case class PhysicalResource(id: PhysicalResourceId, prType: PhysicalResourceType)

object PhysicalResource:
  private val PhysicalResourceIdentifierXmlAttribute = "id"
  private val PhysicalResourceTypeXmlAttribute = "type"

  def from(xml: Node): Result[PhysicalResource] =
    for
      sId <- fromAttribute(xml, PhysicalResourceIdentifierXmlAttribute)
      id <- PhysicalResourceId.from(sId)
      sprType <- fromAttribute(xml, PhysicalResourceTypeXmlAttribute)
      prType <- PhysicalResourceType.from(sprType)
    yield PhysicalResource(id, prType)
