package domain.resource.physical

import domain.Result
import domain.DomainError.InvalidPhysicalId
import domain.Identifier

opaque type PhysicalResourceId = String

object PhysicalResourceId extends Identifier[PhysicalResourceId] :
  def from(s: String): Result[PhysicalResourceId] =
    val regex = "(PRS_)[0-9]+".r
    if (regex.matches(s)) then Right(s) else Left(InvalidPhysicalId(s))
