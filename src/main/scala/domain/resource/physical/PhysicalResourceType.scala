package domain.resource.physical

import domain.DomainError.InvalidPhysicalResourceType
import domain.Result
import domain.SimpleTypes.NonEmptyString
import scala.annotation.targetName


opaque type PhysicalResourceType = String

object PhysicalResourceType:
  def from(s: String): Result[PhysicalResourceType] =
    val regex = "(PRST )[0-9]+".r
    if (regex.matches(s)) then Right(s) else Left(InvalidPhysicalResourceType(s))

extension (x: PhysicalResourceType)
  infix def sameAs(y: PhysicalResourceType): Boolean = x == y

  infix def sameAsString(y: String): Boolean = x == y