package domain.schedule

import domain.order.*
import domain.product.*
import domain.resource.physical.*
import domain.task.*
import domain.resource.human.* 

final case class MS03Struct(orderId: OrderId, productId: ProductId, quantity: Quantity, taskAndPhysicalResources: (Task, List[(PhysicalResource, List[HumanResource])]))
object MS03Struct:

  def from(orderId: OrderId, productId: ProductId, quantity: Quantity, taskAndPhysicalResources: (Task, List[(PhysicalResource, List[HumanResource])])) =
    MS03Struct(orderId, productId, quantity, taskAndPhysicalResources)

