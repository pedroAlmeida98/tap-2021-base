package domain.schedule

import scala.xml.Elem
import domain.DomainError

final case class ScheduleError(domainError: DomainError)

object ScheduleError:

  /**
   * Constructs the xml for the error.
   *
   * @param domainError domain error to print.
   * @return xml representation of the domain error.
   */
  def toXml(domainError: Elem): Elem =
    <ScheduleError xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../scheduleError.xsd "
      message={domainError.toString}>
    </ScheduleError>