package domain.schedule

import scala.xml.Elem
import domain.Result
import domain.DomainError.*
import domain.Production
import domain.schedule.ScheduleError
import domain.schedule.TaskSchedule

import scala.xml.*
import scala.annotation.tailrec
import domain.order.*
import domain.task.*

import scala.util.Try
import domain.product.{Process, Product, ProductId}
import domain.resource.human.*
import domain.resource.physical.*

object ScheduleMS01 extends Schedule :

  /**
   * Creates a production domain object based on a file, running the algorith afterwards
   *
   * @param xml the file to be imported
   * @return an output file with the result of the algorithm
   */
  def create(xml: Elem): Result[Elem] =
    Production.from(xml).fold[Result[Elem]](f => Left(f), r => runAlgorithm(r))

  /**
   * Responsible to return the result of the input file, based on a production domain object
   *
   * @param inboundProduction the production domain model
   * @returns the output file
   */
  def runAlgorithm(inboundProduction: Production): Result[Elem] =
    createTaskSchedules(inboundProduction).fold[Result[Elem]](f => Left(f), r => Right(TaskSchedule.toXmlList(r)))

  /**
   *
   * @param inboundProduction
   * @return
   */
  def createTaskSchedules(inboundProduction: Production): Result[List[TaskSchedule]] =
    handleListOrders(inboundProduction.listPhysicals, inboundProduction.listOrders, inboundProduction.listHumans)
      .fold[Result[List[TaskSchedule]]](f => Left(f), r => Right(r))

  /**
   * Handles each order and, for each one of them, calls the responsible method to create
   * a task schedule
   *
   * @param orders the inbound orders
   * @param lH     the inbound humans that exist on the file
   * @return a list of all task schedules to execute
   */
  def handleListOrders(physicalResources: List[PhysicalResource], orders: List[Order], lH: List[HumanResource]): Result[List[TaskSchedule]] =
    orders.foldLeft[Result[List[TaskSchedule]]](Right(List()))((rListTaskSchedule, currentOrder) =>
      for
        rList <- rListTaskSchedule
        taskSchedule <- rListTaskSchedule.fold[Result[List[TaskSchedule]]](e => Left(e), r =>
          r match
            case Nil => handleSingleOrder(physicalResources, currentOrder, lH, Time.zero())
            case h :: Nil => handleSingleOrder(physicalResources, currentOrder, lH, h.endTime)
            case h :: t => handleSingleOrder(physicalResources, currentOrder, lH, t.last.endTime)
        )
      yield rList ++: taskSchedule
    )

  /**
   * Calls the create task Schedule method recursivly, having in mind some parameters
   *
   * @param order                   the Order that the task schedule will be based from
   * @param lH                      the list of humans for the inbound object
   * @param lastTaskScheduleEndTime initial time for the task to start
   * @return the list of task schedules that exist for an order
   */
  def handleSingleOrder(physicalResources: List[PhysicalResource], order: Order, lH: List[HumanResource], lastTaskScheduleEndTime: Time): Result[List[TaskSchedule]] =
    def recursiveHandleSingleOrder(physicalResources: List[PhysicalResource], quantity: Quantity, startingList: List[TaskSchedule], timeAccumulator: Time): Result[List[TaskSchedule]] =
      addTaskSchedule(physicalResources, order, quantity, lH, timeAccumulator)
        .fold[Result[List[TaskSchedule]]](e => Left(e), s => if (quantity == order.quantity) Right(startingList ++: s) else recursiveHandleSingleOrder(physicalResources, quantity + 1, startingList ++ s, s.last.endTime))

    recursiveHandleSingleOrder(physicalResources, Quantity.zero() + 1, List(), lastTaskScheduleEndTime)

  /**
   * Creates a task schedule with the respective start and end time, product number, taslId, physical and human resources
   *
   * It calls the {@link putTimesOnTaskSchedule} to get the humans available for that task,
   * with the respective physical resources and task. It calls for each task
   * -> {@link getHumanResourceFromTask( )}
   * -> {@link checkHumanResourcesAvailable( )}
   * -> Creates a taskschedule with the aggregate of all
   *
   * @param order                   the order that the task schedules are originated from
   * @param quantity                the quantity that the task schedule refers to
   * @param lH                      the list of available human resources
   * @param lastTaskScheduleEndTime the start time of the task schedule
   * @return the created list of task schedules that originated from an order
   */
  def addTaskSchedule(physicalResources: List[PhysicalResource], order: Order, quantity: Quantity, lH: List[HumanResource], lastTaskScheduleEndTime: Time): Result[List[TaskSchedule]] =
    def recursiveAddTaskSchedule(physicalResources: List[PhysicalResource], startingList: List[TaskSchedule], startTime: Time, productTasks: List[Task]): Result[List[TaskSchedule]] =
      productTasks match
        case Nil => Right(startingList)
        case h :: Nil => putTimesOnTaskSchedule(physicalResources, startingList, h, startTime, List())
        case h :: t => putTimesOnTaskSchedule(physicalResources, startingList, h, startTime, t)

    def putTimesOnTaskSchedule(physicalResources: List[PhysicalResource], startingList: List[TaskSchedule], currentTask: Task, startTime: Time, tasksOfProduct: List[Task]): Result[List[TaskSchedule]] =
      getHumanResourceFromTask(currentTask, lH)
        .fold[Result[List[TaskSchedule]]](error => Left(error), humans => checkHumanResourcesAvailable(humans, currentTask.id, physicalResources)
          .fold[Result[List[TaskSchedule]]](error => Left(error), listHumanResources =>
            recursiveAddTaskSchedule(physicalResources, startingList :+ TaskSchedule.from(order.id, currentTask.id, quantity, startTime, startTime plus currentTask.time, listHumanResources.map(a => a._2), listHumanResources.map(a => a._1)),
              startTime plus currentTask.time, tasksOfProduct)))

    recursiveAddTaskSchedule(physicalResources, List(), lastTaskScheduleEndTime, order.prod.tasks)

  /**
   * Checks if a human resource is available to execute a certain task
   *
   * @param tLHR   the tuple that has the info regarding a task, the human resources available for a task and the correspondent physical resource
   * @param taskId the id of the task to iterate
   * @return the list of human resources available for each task
   */
  def checkHumanResourcesAvailable(tLHR: List[(Task, List[HumanResource], PhysicalResourceType)], taskId: TaskId, physicalResources: List[PhysicalResource]): Result[List[(HumanResource, PhysicalResource)]] =
    def checkHumanResourcesAvailableRecursive(tuples: List[(Task, List[HumanResource], PhysicalResourceType)], listHumanCounter: List[(HumanResource, PhysicalResource)], auxPhysicalresource: List[PhysicalResource]): Result[List[(HumanResource, PhysicalResource)]] =
      tuples match
        case Nil => Right(listHumanCounter)
        case h :: t => iterateHumansForTask(h._2.reverse, h._1.id, h._3, listHumanCounter, auxPhysicalresource)
          .fold(erro => Left(erro), fd => checkHumanResourcesAvailableRecursive(t,  listHumanCounter :+ fd, auxPhysicalresource.filter(p => p.id != fd._2.id)))

    checkHumanResourcesAvailableRecursive(tLHR.reverse, List(), physicalResources)

  
  /**
   * Method that checks if the human resource is available to execute a physical resource on a specific task
   *
   * @param humans           the humans available
   * @param taskId           the task id
   * @param phrType          the type of physical resource to execute
   * @param listHumanCounter e list wthih humans available and capable of executing a task
   * @return the list wthih humans available and capable of executing a task
   */
  def iterateHumansForTask(humans: List[HumanResource], taskId: TaskId, phrType: PhysicalResourceType, listHumanCounter: List[(HumanResource, PhysicalResource)], physicalResource: List[PhysicalResource]): Result[(HumanResource, PhysicalResource)] =
    @tailrec
    def iterateHumansForTaskRecursive(humans: List[HumanResource], listHumanCounter: List[(HumanResource, PhysicalResource)]): Result[(HumanResource, PhysicalResource)] =
      humans match
        case Nil => Left(ResourceUnavailable(taskId.toString, phrType.toString))
        case h :: t => if (listHumanCounter.map(f => f._1.id).contains(h.id)) iterateHumansForTaskRecursive(t, listHumanCounter) else Right((h, physicalResource.filter(pr => pr.prType sameAs phrType).head))

    iterateHumansForTaskRecursive(humans, listHumanCounter)

  /**
   * Checks if a human contains a physical resource
   *
   * @param h      the human to verify
   * @param prType the type of physical resource to check
   * @param task   the task for that physical resource
   * @return a human in case of succces, a domain error in case of failure
   */
  def getHumanResourceByType(h: HumanResource, prType: PhysicalResourceType, task: Task): Result[HumanResource] =
    if (h.physicalResourceTypes.filter(f => f sameAs prType).length == 0) Left(ResourceUnavailable(task.id.toString, prType.toString)) else Right(h)

  /**
   * Iterates each human and call returns all the humans available for the Pysical resource
   *
   * @param lh     the humans to iterate
   * @param prType the tyoe of the pysical resource
   * @param task   the task to iterate
   * @return list of human resources
   */
  def getHumanResources(lh: List[HumanResource], prType: PhysicalResourceType, task: Task): Result[List[HumanResource]] =
    def getHumanResourcesAux(lhR: List[HumanResource], lResult: List[HumanResource]): Result[List[HumanResource]] =
      lhR match
        case Nil => Right(lResult)
        case h :: t => getHumanResourceByType(h, prType, task)
          .fold[Result[List[HumanResource]]](_ => getHumanResourcesAux(t, lResult), _ => getHumanResourcesAux(t, h +: lResult))

    getHumanResourcesAux(lh, List())

  /**
   * Maps a Task with the human resources available for that task as well as the type of the
   * physical resource associated with that task.
   *
   * @param task task to get the human resources from.
   * @param humanResourcesList
   * @return
   */
  def getHumanResourceFromTask(task: Task, humanResourcesList: List[HumanResource]): Result[List[(Task, List[HumanResource], PhysicalResourceType)]] =
    task.physicalResourceTypes.foldLeft[Result[List[(Task, List[HumanResource], PhysicalResourceType)]]](Right(List()))((tupleAcumulator, physicalResourceType) =>
      getHumanResources(humanResourcesList, physicalResourceType, task)
        .fold[Result[List[(Task, List[HumanResource], PhysicalResourceType)]]](error => Left(error), listHumanResources => tupleAcumulator
          .fold(erro => Left(erro), acc => Right((task, listHumanResources, physicalResourceType) +: acc)))
    )