package domain.schedule

//import domain.schedule.ScheduleMS01.*

import scala.xml.Elem
import domain.{Production, Result}
import domain.task.*
import domain.resource.physical.*
import domain.resource.human.*
import domain.DomainError.*
import domain.schedule.*
import domain.order.*
import domain.product.*
import domain.resource.human.HumanResource
import domain.resource.physical.PhysicalResource

import scala.util.Sorting
import scala.annotation.tailrec


object ScheduleMS03 extends Schedule:

  def create(xml: Elem): Result[Elem] =
    Production.from(xml).fold[Result[Elem]](f => Left(f), r => {
      runAlgorithm(r).fold(l2 => Left(l2), r2 => Right(r2))
    })

  def runAlgorithm(inboundProduction: Production): Result[Elem] =
    createTaskSchedules(inboundProduction).fold[Result[Elem]](f => Left(f), r => Right(TaskSchedule.toXmlList(r)))

  def createTaskSchedules(inboundProduction: Production): Result[List[TaskSchedule]] =
    createMS03Struct(inboundProduction)
      .fold(l => Left(l), r => {
        taskScheduleCombinations(r).fold(l2 => Left(l2), r2 => {
          Right(r2)
        })
      })

  def min(s1: TaskSchedule, s2: TaskSchedule): TaskSchedule = if (s1.endTime lessThan s2.endTime) s1 else s2
  def max(s1: TaskSchedule, s2: TaskSchedule): TaskSchedule = if (s1.endTime greaterThan s2.endTime) s1 else s2

  def taskScheduleCombinations(structList: List[MS03Struct]): Result[List[TaskSchedule]] =
    def taskScheduleCombinationsAux(acc: Int, toDoCombinations: List[MS03Struct], doingCombinations: List[MS03Struct], doneCombinations : List[TaskSchedule], time : Time): Result[List[TaskSchedule]] =
      acc match
        case 0 => {
          validateCombinations(List(toDoCombinations), toDoCombinations, doingCombinations, doneCombinations, time).fold(fe => Left(fe),
            r => {

              val resultTaskSchedules = canHumanBeDoneInParalell2(r._2).fold(e => Left(e), gi => {
                val taskSchedules = gi.map(fr =>

                  //val mappedTimesAndHumans : List[(Time, List[HumanResource])] = doneCombinations.map(f => (f.endTime, f.humanResources))
                  //val humanAvailableForTask = h._2.filter(f => !unavailableHuman.map(g => g.id).contains(f.id) && !humansToAdd.map(g => g.id).contains(f.id) && f.physicalResourceTypes.contains(h._1.prType))
                  //fr._2.map(g => g.id)
                  //val x =  mappedTimesAndHumans.head.
                  //
                  /*
                   checkTimesForSchedule(fr, doneCombinations)
                )*/

                  //val minTime = if (doneCombinations.length == 0) Time.zero() else doneCombinations.reduceLeft(min).endTime
                  val maxTime = if (doneCombinations.length == 0) Time.zero() else doneCombinations.reduceLeft(max).endTime

                  TaskSchedule.from(fr._1.orderId, fr._1.taskAndPhysicalResources._1.id, fr._1.quantity, maxTime, maxTime plus fr._1.taskAndPhysicalResources._1.time, fr._1.taskAndPhysicalResources._2.map(lk => lk._1).toList, fr._2))

                Right(taskSchedules)
              })

              resultTaskSchedules.fold(e => Left(e), f => Right(r._3 ::: f))


              //TODO: corrigir tempos das que estão doing e passar para done
              //TODO: reordenar as que começam no mesmo tempo por id
              //resultTaskSchedules.fold(e => Left(e), r => Right(r))
            })
          //          Left(ImpossibleSchedule("ImpossibleSchedule"))
        } //ImpossibleSchedule
        case default =>
          val combinations = toDoCombinations.combinations(acc).toList
          validateCombinations(combinations, toDoCombinations, doingCombinations, doneCombinations, time)
            .fold(l => Left(l), r =>
              if(r._2.length > 0) {
                // retirar das doingCombinations as que ja foram feitas
                taskScheduleCombinationsAux(r._1.length, r._1, r._2, r._3, time)
              } else {
                taskScheduleCombinationsAux(toDoCombinations.length - 1, r._1, r._2, r._3, time)
              })

    /*
    1º iter - List(List(T1,T2,T3))
    2º iter - List(List(T1,T2), List(T1,T3), List(T2,T3))
    3º iter - List(List(T1), List(T2), List(T3))
    */

    /*
    1º iter - T1 -> recursos? S, T2 -> recursos? N, T3
    2º iter - T1 -> recursos? S, T2 -> recursos? N
            - T1 -> recursos? S, T3 -> recursos? N
            - T2 -> recursos? S, T3 -> recursos? S -> alocar
    */

    taskScheduleCombinationsAux(structList.length, structList, List(), List(), Time.zero())

  def validateCombinations(combinations: List[List[MS03Struct]], toDoCombinations: List[MS03Struct], doingCombinations: List[MS03Struct], doneCombinations: List[TaskSchedule], time : Time): Result[(List[MS03Struct], List[MS03Struct], List[TaskSchedule])] =
    combinations match
      case Nil => {
        if(doingCombinations.length <= 0) Right((toDoCombinations, doingCombinations, doneCombinations))
        else
        //val lowestTimerCombinations = doingCombinations//getLowestTimeCombinations(doingCombinations)
        //TODO: Corrigir times
          canHumanBeDoneInParalell2(doingCombinations).fold(e => Left(e), gi => {
            val taskSchedules = gi.map(fr =>
              TaskSchedule.from(fr._1.orderId, fr._1.taskAndPhysicalResources._1.id, fr._1.quantity, time, fr._1.taskAndPhysicalResources._1.time, fr._1.taskAndPhysicalResources._2.map(lk => lk._1).toList, fr._2))

            val consideredCombinations = gi.map(g => g._1)
            val removingDoingFromDoing = doingCombinations.filter(f => !consideredCombinations.map(x => x.orderId).contains(f.orderId) || !consideredCombinations.map(x => x.productId).contains(f.productId) || !consideredCombinations.map(x => x.quantity).contains(f.quantity) || !consideredCombinations.map(x => x.taskAndPhysicalResources._1.id).contains(f.taskAndPhysicalResources._1.id))
            val newToDo = toDoCombinations.filter(f => !removingDoingFromDoing.map(x => x.orderId).contains(f.orderId) || !removingDoingFromDoing.map(x => x.productId).contains(f.productId) || !removingDoingFromDoing.map(x => x.quantity).contains(f.quantity) || !removingDoingFromDoing.map(x => x.taskAndPhysicalResources._1.id).contains(f.taskAndPhysicalResources._1.id))
            val tuple = (newToDo, removingDoingFromDoing, doneCombinations ::: taskSchedules)
            Right(tuple)
          })
        // adicionar o tempo às outras
        //Left(ImpossibleSchedule("ImpossibleSchedule")) //ImpossibleSchedule
      }
      case h :: t => areCombinationsValid(h, doingCombinations).fold(e => Left(e),
        r => {
          if (r) {
            val i = toDoCombinations.filter(f => !h.map(x => x.orderId).contains(f.orderId) || !h.map(x => x.productId).contains(f.productId) || !h.map(x => x.quantity).contains(f.quantity) || !h.map(x => x.taskAndPhysicalResources._1.id).contains(f.taskAndPhysicalResources._1.id) )
            Right((i, doingCombinations ::: h, doneCombinations))
          }
          else validateCombinations(t, toDoCombinations, doingCombinations, doneCombinations, time)
        })

  def areCombinationsValid(combination: List[MS03Struct], doingCombinations: List[MS03Struct]): Result[Boolean] =
    canHumanBeDoneInParalell(combination).fold(f => Left(f), r => {
      if (r) {
        val result = canAllHumanBeDoneInParalellWithDoingCombinations(combination, doingCombinations).fold[Result[Boolean]](f => Left(f), rd => {
          //TODO: more validations
          Right(rd)
        })
        result
      }
      else {
        Right(false)
      }
    })

  def canHumanBeDoneInParalell(combination: List[MS03Struct]): Result[Boolean] =
    def canBeDoneInParalellRec(combination: List[MS03Struct], accumulator: List[HumanResource]): Result[Boolean] =
      combination match
        case Nil => Right(true)
        case h :: t => {
          canHumanBeDone(h.taskAndPhysicalResources._2, accumulator)
            .fold(fa => Right(false), fd => canBeDoneInParalellRec(t, accumulator ::: fd))
        }
    canBeDoneInParalellRec(combination, List())

  def canHumanBeDone(listToCheckList: List[(PhysicalResource, List[HumanResource])], unavailableHuman: List[HumanResource]): Result[List[HumanResource]] =
    def canHumanBeDoneRec(listToCheckList: List[(PhysicalResource, List[HumanResource])], humansToAdd: List[HumanResource]): Result[List[HumanResource]] =
      listToCheckList match
        case Nil => Right(humansToAdd)
        case h :: t =>
          val humanAvailableForTask = h._2.filter(f => !unavailableHuman.map(g => g.id).contains(f.id) && !humansToAdd.map(g => g.id).contains(f.id) && f.physicalResourceTypes.contains(h._1.prType))
          if(humanAvailableForTask.length <= 0) Left(ImpossibleSchedule("ImpossibleSchedule")) else
            canHumanBeDoneRec(t, humansToAdd :+ humanAvailableForTask.head) //humanAvailableForTask.take(listToCheckList.length))

    canHumanBeDoneRec(listToCheckList, List())


  def canAllHumanBeDoneInParalellWithDoingCombinations(combination: List[MS03Struct], doingCombinations: List[MS03Struct]): Result[Boolean] =
    if(doingCombinations.length <= 0) Right(true) else canHumanBeDoneInParalell(combination:::doingCombinations)



  def canHumanBeDoneInParalell2(combination: List[MS03Struct]): Result[List[(MS03Struct,List[HumanResource])]] =
    def canBeDoneInParalellRec2(combination: List[MS03Struct], accumulator: List[(MS03Struct,List[HumanResource])]): Result[List[(MS03Struct,List[HumanResource])]] =
      combination match
        case Nil => Right(accumulator)
        case h :: t => {
          val x =  accumulator.filter(ac => getMappedHumansForDoingCombination(ac._1, h)).flatMap(a => a._2)
          canHumanBeDone(h.taskAndPhysicalResources._2, accumulator.filter(ac => getMappedHumansForDoingCombination(ac._1, h)).flatMap(a => a._2))
            .fold(fa => Left(fa), fd => canBeDoneInParalellRec2(t, accumulator :+ (h, fd)))
        }
    canBeDoneInParalellRec2(combination, List())

  def getMappedHumansForDoingCombination(doneCombination: MS03Struct, doingCombination: MS03Struct): Boolean =
    !(doneCombination.orderId sameOrderId doingCombination.orderId) ||
      !(doneCombination.productId same doingCombination.productId) ||
      !(doneCombination.quantity == doingCombination.quantity) ||
      !(doneCombination.taskAndPhysicalResources._1.id sameTaskId doingCombination.taskAndPhysicalResources._1.id)


  def getLowestTimeCombinations(doingCombinations: List[MS03Struct]): List[MS03Struct] =
    val orderedList = doingCombinations.sortWith(_.taskAndPhysicalResources._1.time lessThan _.taskAndPhysicalResources._1.time)
    orderedList.filter(u => u.taskAndPhysicalResources._1.time == orderedList.head.taskAndPhysicalResources._1.time)


  def createMS03Struct(production: Production) : Result[List[MS03Struct]] =
    @tailrec
    def handleMS03StructAux(orders: List[Order], list: List[MS03Struct]): Result[List[MS03Struct]] =
      orders match
        case Nil => Right(list)
        case h :: t => {
          val s =  iterateTask(h, h.quantity, production.listPhysicals, production.listHumans)
          handleMS03StructAux(t, list ::: s)
        }
    handleMS03StructAux(production.listOrders, List())

  def iterateTask(order: Order, quantity: Quantity, physicalResources: List[PhysicalResource], humanResources: List[HumanResource]): List[MS03Struct] =
    order.prod.tasks.foldLeft[List[MS03Struct]](List())((listToAdd, task) => {

      def getPhysicalResourcesForTask: List[PhysicalResource] =
        @tailrec
        def getTypesForTaskAux(physicalResourceTypes: List[PhysicalResourceType], acc: List[PhysicalResource]): List[PhysicalResource] =
          physicalResourceTypes match
            case Nil => acc
            case h :: t => getTypesForTaskAux(t, acc ::: physicalResources.filter(p => p.prType sameAs h))

        getTypesForTaskAux(task.physicalResourceTypes, List())

      def getHumanResourcesForTask(phrType: PhysicalResourceType): List[HumanResource] =
        def getHumanResourcesForTaskAux(aux: List[HumanResource]): List[HumanResource] =
          aux ::: humanResources.foldLeft[List[HumanResource]](List())((listToAdd, human) =>
            if (human.physicalResourceTypes.filter(phrType => phrType sameAs phrType).length > 0) listToAdd :+ human else listToAdd)

        getHumanResourcesForTaskAux(List())

      def createMS03Struct: List[MS03Struct] =
        @tailrec
        def createMS03Aux(acc: Quantity, aux: List[MS03Struct]): List[MS03Struct] =
          if (order.quantity == acc) aux else
            createMS03Aux(acc + 1,
              aux :+ MS03Struct.from(order.id, order.prod.id, acc + 1, (task, getPhysicalResourcesForTask.map(p => (p, getHumanResourcesForTask(p.prType))))))

        createMS03Aux(Quantity.zero(), List())

      listToAdd ::: createMS03Struct
    })