package domain.schedule

import domain.DomainError.*
import domain.SimpleTypes.*
import domain.resource.human.*
import domain.task.{Task, TaskId, Time}
import domain.order.{Order, OrderId, Quantity}
import domain.handle.*
import domain.resource.physical.*
import domain.Result

import java.time.format.DateTimeFormatter
import scala.xml.{Elem, Node}


final case class TaskSchedule(orderId: OrderId, taskId: TaskId, quantity: Quantity, startTime: Time, endTime: Time,
                              physicalResources: List[PhysicalResource], humanResources: List[HumanResource])

object TaskSchedule:

  def from(orderId: OrderId, taskId: TaskId, quantity: Quantity, startTime: Time, endTime: Time, physicalResources: List[PhysicalResource], humanResources: List[HumanResource]) =
    TaskSchedule(orderId, taskId, quantity, startTime, endTime, physicalResources, humanResources)

  /**
   * Constructs a XML representation of the Schedule.
   *
   * @param taskSchedules list of task schedules to convert.
   * @return xml representation of the schedule.
   */
  def toXmlList(taskSchedules: List[TaskSchedule]): Elem =
    <Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
      {taskSchedules.map(h => toXmlListTaskSchedule(h))}
    </Schedule>

  /**
   * Constructs a XML representation of the task schedule.
   *
   * @param taskSchedule task schedule to convert.
   * @return xml representation of the task schedule.
   */
  private def toXmlListTaskSchedule(taskSchedule: TaskSchedule): Elem =
    <TaskSchedule order={taskSchedule.orderId.toString} productNumber={taskSchedule.quantity.toString} task={taskSchedule.taskId.toString} start={taskSchedule.startTime.toString} end={taskSchedule.endTime.toString}>
      <PhysicalResources>
        {taskSchedule.physicalResources.map(pr => toXmlPhysicalResource(pr))}
      </PhysicalResources>
      <HumanResources>
        {taskSchedule.humanResources.map(pr => toXmlHumanResource(pr))}
      </HumanResources>
    </TaskSchedule>

  /**
   * Constructs a XML representation of the physical resource.
   *
   * @param physicalResource physical resource to convert.
   * @return xml representation of the physical resource.
   */
  private def toXmlPhysicalResource(physicalResource: PhysicalResource): Node =
    <Physical id={physicalResource.id.toString}/>

  /**
   * Constructs a XML representation of the human resource.
   *
   * @param humanResource human resource to convert.
   * @return xml representation of the human resource.
   */
  private def toXmlHumanResource(humanResource: HumanResource): Elem =
    <Human name={humanResource.name}/>



