package domain.task
import domain.Result
import domain.resource.physical.*
import domain.DomainError.*
import domain.SimpleTypes.*
import domain.task.{TaskId, Time}
import scala.xml.{Elem, Node}
import scala.util.Try
import domain.handle.Handle
import domain.resource.human.*
import xml.XML.*
import scala.annotation.tailrec

final case class Task(id: TaskId, time: Time, physicalResourceTypes: List[PhysicalResourceType])

object Task:
  private val PhysicalResourceIdentifierXmlAttribute = "id"
  private val TimeXmlAttribute = "time"
  private val PhysicalResourceXmlNode = "PhysicalResource"

  def from(physicalresources: List[PhysicalResource])(xml: Node): Result[Task] =
    for
      sTaskId <- fromAttribute(xml, PhysicalResourceIdentifierXmlAttribute)
      taskId <- TaskId.from(sTaskId)
      integerTime <- fromAttribute(xml, TimeXmlAttribute)
      taskTime <- Time.from(integerTime)
      listPhysicalResource <- traverse((xml \\ PhysicalResourceXmlNode), Handle.from(physicalresources))
    yield Task(taskId, taskTime, listPhysicalResource.map(h => h.pr.prType))
