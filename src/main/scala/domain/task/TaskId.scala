package domain.task

import domain.Result
import domain.DomainError.*
import domain.Identifier
import scala.annotation.targetName

opaque type TaskId = String

object TaskId extends Identifier[TaskId] :
  def from(t: String): Result[TaskId] =
    val validRegex = "(TSK_)[0-9]+".r
    if (validRegex.matches(t)) then Right(t) else Left(InvalidTaskId(t))

extension (x: TaskId)
  infix def sameTaskId(xs: TaskId): Boolean = x == xs
