package domain.task

import domain.Result
import domain.SimpleTypes.*

import scala.util.Try
import domain.DomainError.*
import domain.SimpleTypes.NonNegativeInteger.*

opaque type Time = NonNegativeInteger

object Time:
  private def from(i: Int): Result[Time] =
    NonNegativeInteger.from(i)

  def to(i: Time): Int =
    NonNegativeInteger.to(i)

  def from(s: String): Result[Time] =
    Try(s.toInt).fold[Result[Time]](_ => Left(InvalidTime(s)), p => from(p.toInt))

  def zero(): Time = NonNegativeInteger.zero()

extension (x: Time)
  infix def plus(xs: Time): Time = x + xs

  infix def lessThan(xs: Time): Boolean = x < xs

  infix def lessEqualThan(xs: Time): Boolean = x <= xs

  infix def greaterThan(xs: Time): Boolean = x > xs

  infix def greaterEqualThan(xs: Time): Boolean = x >= xs