package assessment

class CustomTestMS03 extends AssessmentBehaviours:
  
  val PATH = "files/assessment/ms03CustomTests"   // Assessment file path
  performTests(assessment.AssessmentMS03.create, "Milestone 3 own tests")

