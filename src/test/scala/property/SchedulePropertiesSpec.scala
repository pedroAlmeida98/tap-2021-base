package property

import scala.language.adhocExtensions
import org.scalacheck.{Arbitrary, Properties}
import org.scalatest.matchers.should.Matchers
import org.scalatest.propspec.AnyPropSpec
import org.scalacheck.Prop.{forAll, propBoolean}
import property.generator.ScheduleGenerator.genProduction
import sun.font.Type1Font
import scala.annotation.tailrec

import domain.Production
import domain.resource.human.HumanResource
import domain.schedule.{TaskSchedule, ScheduleMS01}
import domain.resource.physical.*
import domain.product.*
import domain.order.*
import domain.task.*

class SchedulePropertiesSpec extends Properties("Schedule") :

  property("The complete schedule must schedule all the tasks of all the products needed.") =
    forAll(genProduction)((production: Production) =>
      val b = createTuple(production.listOrders)

      ScheduleMS01.createTaskSchedules(production).fold[Boolean](_ => false, r => {
        val f = r.foldLeft[List[(OrderId, TaskId)]](List())((listToAdd, taskSchedule) =>
          listToAdd.::((taskSchedule.orderId, taskSchedule.taskId))
        )
        b.length == f.length && b.toSet.equals(f.toSet)
      }
    )
  )

  property("No overlapping times") =
    forAll(genProduction)((production) =>
      ScheduleMS01.createTaskSchedules(production).fold(_ => false, taskSchedule => {
        // Map (start, end) of all times
        !intervalsOverlapping(taskSchedule.map(f => (f.startTime, f.endTime)))
      })
    )

  property("Same resource cannot be used at the same time by two tasks") =
    forAll(genProduction)((production) =>
      val algResult = ScheduleMS01.createTaskSchedules(production)
        algResult.fold(_ => false,
      tsList => {
        //Ignore the same task schedule for the mapping list
        val keyValues = tsList.map(f => (f, tsList.filter(g => g.orderId != f.orderId || g.taskId != f.taskId || g.quantity != g.quantity)))
        taskSchedulesRunning(keyValues)
      })
  )

  property("Same tasks ordered from product are the same ordered in Schedule") =
    forAll(genProduction)((production: Production) =>
      ScheduleMS01.createTaskSchedules(production)
        .fold[Boolean](_ => false, result => createTaskProductTuple(production) == createTaskProductTuple(result))
    )

  property("Task schedules times sequencial") =
    forAll(genProduction)((production: Production) =>
      ScheduleMS01.createTaskSchedules(production).fold[Boolean](_ => false, r => areTimesSequencial(r)
      )
    )

  /**
   * Maps a list of task schedules to a tuple of order id, product number and task id for the given order.
   *
   * @param taskSchedules list of task schedules.
   * @return list of tuples.
   */
  private def createTaskProductTuple(taskSchedules: List[TaskSchedule]): List[(OrderId, Integer, TaskId)] =
    taskSchedules.map(taskSchedule => (taskSchedule.orderId, Quantity.to(taskSchedule.quantity), taskSchedule.taskId))

  /**
   * Maps a production to a tuple of order id, product number and task id for the given order.
   *
   * @param production production to map.
   * @return list of tuples.
   */
  private def createTaskProductTuple(production: Production): List[(OrderId, Integer, TaskId)] =
    @tailrec
    def createTaskProductTupleRec(orders: List[Order], accumulator: List[(OrderId, Integer, TaskId)]): List[(OrderId, Integer, TaskId)] =
      orders match
        case Nil => accumulator
        case h :: t => createTaskProductTupleRec(t, accumulator ::: getTupleForOrder(h))

    createTaskProductTupleRec(production.listOrders, List())

  /**
   * Creates a tuple of order id, product number and task id for the given order.
   *
   * @param order order to create the tuple.
   * @return list of tuples.
   */
  private def getTupleForOrder(order: Order): List[(OrderId, Integer, TaskId)] =
    (1 to Quantity.to(order.quantity)).map[List[(OrderId, Integer, TaskId)]](productNumber =>
      order.prod.tasks.map[(OrderId, Integer, TaskId)](task => (order.id, productNumber, task.id))
    ).toList.flatten

  /**
   * Creates a list of n tuples with the order and task id, being n the number of quantities for each order
   *
   * @param orders the existing orders
   * @return a list of n tuples, being n the result of the quantities and the existant tasks of the product
   */
  private def createTuple(orders: List[Order]): List[(OrderId, TaskId)] =
    def createTupleAux(orders: List[Order], acc: Int, list: List[(OrderId, TaskId)]): List[(OrderId, TaskId)] =
      orders match
        case Nil => list
        case h :: t => createTupleAux(t, acc, list ::: iterateTask(h, acc, list))

    createTupleAux(orders, 0, List())

  /**
   * Auxiliary method for {@link createTuple} that iterates to each task and adds an element to the list of tuples
   *
   * @param order the order that contains the products
   * @param acc   the accumulator to accumulate the values
   * @param list  the list to be returned that serves as a stack
   * @return a list of tuples, for each order
   */
  private def iterateTask(order: Order, acc: Int, list: List[(OrderId, TaskId)]): List[(OrderId, TaskId)] =
    val a = order.quantity plusQuantity acc

    order.prod.tasks.foldLeft[List[(OrderId, TaskId)]](List())((listToAdd, task) =>
      listToAdd ::: List.tabulate(a)(n => (order.id, task.id))
    )

  /**
   * Checks if two intervals are overlapping.
   *
   * Example:
   * isOverlapping(0, 100, 100, 200) --> false
   * isOverlapping(0, 100, 90, 200) --> true
   * isOverlapping(50, 100, 0, 60) --> true
   * isOverlapping(50, 100, 0, 200) --> true
   * isOverlapping(100, 200, 0, 100) --> false
   *
   * @param startTime1 first interval start time.
   * @param endTime1   first interval end time
   * @param startTime2 second interval start time
   * @param endTime2   second interval end time
   * @return true if overlapping, false otherwise.
   */
  private def isOverlapping(startTime1: Time, endTime1: Time, startTime2: Time, endTime2: Time): Boolean =
    if (startTime1 lessThan startTime2)
      ((endTime1 greaterThan startTime2) || (endTime2 lessThan startTime1))
    else // If somewhow, the second interval start time is less than the first one
      ((endTime2 greaterThan startTime1) || (endTime1 lessThan startTime2))

  /**
   * Check if theres any intervals overlapping on a list of intervals.
   *
   * @param times list of intervals
   * @return true if theres at least of overlapping, false otherwise.
   */
  private def intervalsOverlapping(times: List[(Time, Time)]): Boolean =
    @tailrec
    def intervalsOverlappingRec(initialList: List[(Time, Time)], timesIterator: List[(Time, Time)]): Boolean =
      timesIterator match
        case Nil => false
        case h :: t =>
          // "remove" the time that doesn't need to be compared for overlap
          val listWithoutSearchElem = initialList.filter(interval => interval._1 != h._1 && interval._2 != h._2)
          val overlappingList = listWithoutSearchElem.filter(interval => isOverlapping(h._1, h._2, interval._1, interval._2))
          // If there is someoverlapping then the overlappingList is filled
          if (overlappingList.length > 0) true
          else intervalsOverlappingRec(initialList, t)

    intervalsOverlappingRec(times, times)

  /**
   * Checks if the times on a list of task schedules are sequencial.
   * Meaning there is no gaps between task schedules.
   *
   * @param taskSchedules list to evaluate if task schedules are sequencial.
   * @return true if the task schedules are sequencial, false otherwise.
   */
  @tailrec
  private def areTimesSequencial(taskSchedules: List[TaskSchedule]): Boolean =
    taskSchedules match
      case Nil => true
      case h :: Nil => true
      case h :: t => if (h.startTime == t.head.endTime) false else areTimesSequencial(t)

  /**
   * Checks if there is the same resource in two overlapping task schedules.
   *
   * @param taskSchedule  task schedule to verify.
   * @param taskSchedules list of all task schedules.
   * @return true if there is at least one task schedule overlapping the task schdule passed by parameter
   *         and with the same resource, false otgherwise.
   */
  @tailrec
  private def taskSchedulesOverlappingSameResource(taskSchedule: TaskSchedule, taskSchedules: List[TaskSchedule]): Boolean =
    taskSchedules match
      case Nil => true
      case h :: t =>
        if (isOverlapping(taskSchedule.startTime, taskSchedule.endTime, h.startTime, h.endTime))
        //Human Id not exists on list of taskschedules
          !taskSchedule.humanResources.exists(humanResource1 => h.humanResources.exists(humanResource2 => humanResource2.id == humanResource1.id))
        else taskSchedulesOverlappingSameResource(taskSchedule, t)

  /**
   * Iterates over the tuple of the task schedule.
   *
   * @param list list of task schedule tuple.
   * @return true if theres no overlapping of resources, false otherwise.
   */
  private def taskSchedulesRunning(list: List[(TaskSchedule, List[TaskSchedule])]): Boolean =
    list match
      case Nil => true
      case h :: t => taskSchedulesOverlappingSameResource(h._1, h._2)

  /*
  property("Dummy") =
    val node1 : Node = <Physical id="PRS_1" type="PRST 1"/>
    val node2 : Node = <Physical id="PRS_2" type="PRST 2"/>
    val hnode1 : Node = <Human id="HRS_1" name="Antonio">
        <Handles type="PRST 1"/>
        </Human>
    val hnode2 : Node = <Human id="HRS_2" name="Jose">
      <Handles type="PRST 2"/>
      </Human>

    val lPh : List[PhysicalResource] = List(
      PhysicalResource.from(node1).getOrElse(null),
      PhysicalResource.from(node2).getOrElse(null)
    )

    val lH : List[HumanResource] = List(
      HumanResource.from(lPh)(hnode1).getOrElse(null),
      HumanResource.from(lPh)(hnode2).getOrElse(null)
    )

    val tsList : List[TaskSchedule] = List(
      TaskSchedule.from(OrderId.from("ORD_1").right.get, TaskId.from("TSK_1").right.get, Quantity.from("1").right.get, Time.from("0").right.get, Time.from("100").right.get, lPh, lH),
      TaskSchedule.from(OrderId.from("ORD_1").right.get, TaskId.from("TSK_2").right.get, Quantity.from("1").right.get, Time.from("0").right.get, Time.from("100").right.get, lPh, lH)
    )

    val keyValues = tsList.map(f => (f, tsList.filter(g => g.orderId != f.orderId || g.taskId != f.taskId || g.quantity != g.quantity)))
      /*val keyValues = tsList.foldLeft[List[(TaskSchedule, List[TaskSchedule])]](List())((listToAdd, elem) => {
      println(listToAdd)
      println(elem)
      listToAdd.:: (elem, tsList.filter(g => g.orderId != elem.orderId || g.taskId != elem.taskId || g.quantity != elem.quantity))
    })*/

    println(keyValues)
    taskSchedulesRunning(keyValues)
*/