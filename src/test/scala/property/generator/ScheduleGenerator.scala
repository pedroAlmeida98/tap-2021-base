package property.generator

import domain.Production
import org.scalacheck.Gen
import property.generator.handle.HandleGenerator.genHandles
import property.generator.order.OrderGenerator.genOrders
import property.generator.product.ProductGenerator.genProducts
import property.generator.resource.human.HumanResourceGenerator.genHumanResources
import property.generator.resource.physical.PhysicalResourceGenerator.genPhysicalResources
import property.generator.task.TaskGenerator.genTasks

object ScheduleGenerator:

  /**
   * Generates a production.
   *
   * @return a production with generated tasks, orders, products and humans.
   */
  def genProduction: Gen[Production] =
    for
      physicalResources <- genPhysicalResources
      humans <- genHumanResources(physicalResources.map(p => p.prType))
      tasks <- genTasks(humans.flatMap(h => h.physicalResourceTypes))
      products <- genProducts(tasks)
      orders <- genOrders(products)
    yield Production(tasks, orders, products, humans, physicalResources)
