package property.generator

import org.scalacheck.Gen

import domain.SimpleTypes.NonEmptyString

object SimpleGenerators:

  private val MAX_TIME = 100
  val STRING_LENGTH = 10
  val MAX_DECIMAL_CASES = "%02d"

  /**
   * Generates a non negative integer.
   *
   * @return non negative integer.
   */
  def genNonNegativeInteger: Gen[Int] = Gen.chooseNum(1, MAX_TIME)

  /**
   * Generates a list of n elements.
   *
   * @param ammount number of elements to create.
   * @return list with n products
   */
  def genListElements(ammount: Int): Gen[Int] = Gen.chooseNum(1, ammount)

  /**
   * Generates a non empty string.
   *
   * @return random non empty string.
   */
  def genNonEmptyString: Gen[NonEmptyString] =
    for
      genStr <- Gen.listOfN(STRING_LENGTH, Gen.alphaChar)
      str <- NonEmptyString.from(genStr.mkString).fold(_ => Gen.fail, res => Gen.const(res))
    yield str
