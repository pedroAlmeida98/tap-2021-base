package property.generator.handle

import org.scalacheck.Gen

import domain.handle.Handle
import domain.resource.physical.PhysicalResource
import property.generator.SimpleGenerators.genListElements
import property.generator.resource.human.HumanResourceGenerator.MAX_HUMAN_RESOURCES

object HandleGenerator:

  /**
   * Generates a list of handles for the given physical resources.
   *
   * @param physicalResources physical resources to generate the handle from.
   * @return list of handles.
   */
  def genHandles(physicalResources: List[PhysicalResource]): Gen[List[Handle]] =
    for
      numberElements <- genListElements(MAX_HUMAN_RESOURCES)
      handle = physicalResources.map(phr => genHandle(phr))
      handles <- Gen.sequence[List[Handle], Handle](handle)
    yield handles

  /**
   * Generates a handle for the given physical resource.
   *
   * @param physicalResource physical resource to generate the handle from.
   * @return newly generated handle.
   */
  private def genHandle(physicalResource: PhysicalResource): Gen[Handle] =
    for
      handle <- Gen.oneOf(List(physicalResource).toIterable)
    yield Handle(handle)
