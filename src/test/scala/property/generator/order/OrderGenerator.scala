package property.generator.order

import org.scalacheck.Gen

import domain.order.{Order, OrderId}
import domain.product.Product
import property.generator.order.QuantityGenerator.genQuantity
import property.generator.product.ProductGenerator.MAX_PRODUCTS
import property.generator.SimpleGenerators.{genListElements, MAX_DECIMAL_CASES}

object OrderGenerator:

  private val INDEX = "ORD_"
  private val INITIAL_ID_NUMBER = 1

  /**
   * Generates a list of orders for the given products.
   *
   * @param products products to genereate the orders from.
   * @return list of orders.
   */
  def genOrders(products: List[Product]): Gen[List[Order]] =
    for
      n <- genListElements(MAX_PRODUCTS)
      ids = (INITIAL_ID_NUMBER to MAX_PRODUCTS).map(s => INDEX + MAX_DECIMAL_CASES.format(s)).toList
      orderIdsProducts = ids zip products
      order = orderIdsProducts.map[Gen[Order]](orderIdProduct => genOrder(orderIdProduct._1, orderIdProduct._2))
      orders <- Gen.sequence[List[Order], Order](order)
    yield orders

  /**
   * Generates a single order.
   *
   * @param id      order identifier.
   * @param product product to generate an order from.
   * @return newly generated order.
   */
  private def genOrder(id: String, product: Product): Gen[Order] =
    for
      orderId <- OrderId.from(id).fold(_ => Gen.fail, res => Gen.const(res))
      quantity <- genQuantity
    yield Order(orderId, product, quantity)
