package property.generator.order

import domain.order.Quantity
import org.scalacheck.Gen

object QuantityGenerator:

  private val MINIMUM = 1
  private val MAXIMUM = 1

  /**
   * Generates a quantity.
   *
   * @return new quantity.
   */
  def genQuantity: Gen[Quantity] =
    for
      number <- Gen.chooseNum(MINIMUM, MAXIMUM)
      quantity <- Quantity.from(number).fold(_ => Gen.fail, result => Gen.const(result))
    yield quantity