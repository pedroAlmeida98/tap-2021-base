package property.generator.product

import domain.product.FullName
import org.scalacheck.Gen

object FullNameGenerator:

  private val fullNames = List("AZEITE", "OLEO ALIMENTAR", "ARROZ", "AÇUCAR", "FARINHAS", "MASSAS SECAS", "PAPAS DE FLOCOS")

  /**
   * Generates a full name for the product.
   *
   * @return product full name.
   */
  def genFullName: Gen[FullName] =
    for
      fullNameString <- Gen.oneOf(fullNames)
      fullName <- FullName.from(fullNameString).fold(_ => Gen.fail, result => Gen.const(result))
    yield fullName