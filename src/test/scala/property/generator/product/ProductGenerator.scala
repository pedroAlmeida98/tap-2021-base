package property.generator.product

import org.scalacheck.Gen

import domain.product.{Product, ProductId}
import domain.task.Task
import property.generator.SimpleGenerators.{genListElements, MAX_DECIMAL_CASES}
import property.generator.product.FullNameGenerator.genFullName

object ProductGenerator:

  private val INDEX = "PRD_"
  private val INITIAL_ID_NUMBER = 1

  val MAX_PRODUCTS = 2

  /**
   * Generates a list of products for the given tasks.
   *
   * @param tasks tasks to be used in the products generation
   * @return list of products
   */
  def genProducts(tasks: List[Task]): Gen[List[Product]] =
    for
      numberProducts <- genListElements(MAX_PRODUCTS)
      ids = (INITIAL_ID_NUMBER to numberProducts).map(s => INDEX + MAX_DECIMAL_CASES.format(s)).toList
      product = ids.map[Gen[Product]](h => genProduct(h, tasks))
      products <- Gen.sequence[List[Product], Product](product)
    yield products

  /**
   * Generates a single product for the given tasks.
   *
   * @param id    product identifier
   * @param tasks tasks that can be added to the product
   * @return list of products.
   */
  private def genProduct(id: String, tasks: List[Task]): Gen[Product] =
    for
      productId <- ProductId.from(id).fold(_ => Gen.fail, res => Gen.const(res))
      name <- genFullName
      tasksSubset <- Gen.atLeastOne(tasks)
    yield Product(productId, name, tasksSubset.toList)
