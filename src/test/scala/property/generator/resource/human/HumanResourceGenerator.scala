package property.generator.resource.human

import org.scalacheck.Gen

import domain.handle.Handle
import domain.resource.human.{HumanResource, HumanResourceId}
import domain.resource.physical.*
import property.generator.SimpleGenerators.{genListElements, MAX_DECIMAL_CASES}

object HumanResourceGenerator:

  private val HUMAN_RESOURCE_INDEX = "HRS_"
  private val names = List("Antonio", "Maria", "Jose", "Jorge Jesus", "Vitor Baía")

  val MAX_HUMAN_RESOURCES = 2

  /**
   * Generates a list of human resources with a given list of ophysical resources to handle
   *
   * @param physicalResources the physical resources to be handle by the humans
   * @return the list of valid human resources
   */
  def genHumanResources(physicalResources: List[PhysicalResourceType]): Gen[List[HumanResource]] =
    for
      n <- genListElements(MAX_HUMAN_RESOURCES)
      ids = (1 to n).map(s => HUMAN_RESOURCE_INDEX + MAX_DECIMAL_CASES.format(s)).toList
      humanResource = ids.map[Gen[HumanResource]](h => genHumanResource(h, physicalResources))
      humanResources <- Gen.sequence[List[HumanResource], HumanResource](humanResource)
    yield humanResources

  /**
   * Generates a human resource based on an ID and a list ofphysical resources to handle
   *
   * @param id                the id of the human resource
   * @param physicalResources the list of physical resources to handle by the human
   * @return a valid human
   */
  private def genHumanResource(id: String, physicalResources: List[PhysicalResourceType]): Gen[HumanResource] =
    for
      hid <- HumanResourceId.from(id).fold(_ => Gen.fail, res => Gen.const(res))
      name <- Gen.oneOf(names)
      physicalResources <- Gen.atLeastOne(physicalResources)
    yield HumanResource(hid, name, physicalResources.toList)

