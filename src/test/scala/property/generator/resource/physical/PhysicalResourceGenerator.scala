package property.generator.resource.physical

import org.scalacheck.Gen

import domain.resource.physical.{PhysicalResource, PhysicalResourceId, PhysicalResourceType}
import property.generator.SimpleGenerators.{genListElements, MAX_DECIMAL_CASES}

object PhysicalResourceGenerator:

  private val PHYSCIAL_RESOURCE_TYPE = "PRST "
  private val PHYSICAL_RESOURCE_INDEX = "PRS_"
  private val MAX_PHYSICAL_RESOURCES = 5
  private val INITIAL_ID_NUMBER = 1

  /**
   * Generatesa list of valid physical resources.
   *
   * @return the physical resources.
   */
  def genPhysicalResources: Gen[List[PhysicalResource]] =
    for
      n <- genListElements(MAX_PHYSICAL_RESOURCES)
      ids = (INITIAL_ID_NUMBER to n).map(s => PHYSICAL_RESOURCE_INDEX + MAX_DECIMAL_CASES.format(s)).toList
      phrtype = (INITIAL_ID_NUMBER to n).map(s => PHYSCIAL_RESOURCE_TYPE + MAX_DECIMAL_CASES.format(s)).toList
      tuples = pairUpLists(ids, phrtype)
      physicalResource = tuples.map[Gen[PhysicalResource]]((h, j) => genPhysicalResource(h, j))
      physicalResources <- Gen.sequence[List[PhysicalResource], PhysicalResource](physicalResource)
    yield physicalResources

  /**
   * Generates a valid physical resource based on an ID and a type.
   *
   * @param id      the physcial resource identifier
   * @param phrType the type of the physical resource
   * @return a valid physical resource.
   */
  private def genPhysicalResource(id: String, phrType: String): Gen[PhysicalResource] =
    for
      phyid <- PhysicalResourceId.from(id).fold(_ => Gen.fail, res => Gen.const(res))
      phtype <- PhysicalResourceType.from(phrType).fold(_ => Gen.fail, res => Gen.const(res))
    yield PhysicalResource(phyid, phtype)

  /**
   * Creates a list of tuples based on two lists.
   *
   * @param list1 the first list of the tuple
   * @param list2 the second list of the tuple
   * @return a list of tuples containing the two lists.
   */
  private def pairUpLists(list1: List[String], list2: List[String]): List[(String, String)] =
    (for {
      a <- list1
      b <- list2
    } yield (a, b))

