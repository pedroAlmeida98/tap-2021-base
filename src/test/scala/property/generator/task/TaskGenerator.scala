package property.generator.task

import org.scalacheck.Gen
import domain.handle.Handle
import domain.resource.physical.PhysicalResource
import domain.task.{Task, TaskId}
import domain.resource.physical.*
import property.generator.SimpleGenerators.genListElements
import property.generator.task.TimeGenerator.genTime

object TaskGenerator:

  private val TASK_INDEX = "TSK_"
  private val MAX_DECIMAL_CASES = "%02d"
  private val MAX_TASKS = 3
  private val INITIAL_ID_NUMBER = 1

  /**
   * Generates a list of tasks with a given list of physicalResources.
   *
   * @param physicalResources the physicalResources to be used on a task
   * @return the list of tasks.
   */
  def genTasks(physicalResources: List[PhysicalResourceType]): Gen[List[Task]] =
    for
      n <- genListElements(MAX_TASKS)
      ids = (INITIAL_ID_NUMBER to n).map(s => TASK_INDEX + MAX_DECIMAL_CASES.format(s)).toList
      task = ids.map[Gen[Task]](h => genTask(h, physicalResources))
      tasks <- Gen.sequence[List[Task], Task](task)
    yield tasks

  /**
   * Generates a single task given an ID and a list oh physical resouces to handle.
   *
   * @param id                the task id
   * @param physicalResources the physical resources that the task can have
   * @return a task.
   */
  private def genTask(id: String, physicalResources: List[PhysicalResourceType]): Gen[Task] =
    for
      taskId <- TaskId.from(id).fold(_ => Gen.fail, res => Gen.const(res))
      time <- genTime
      prTypes <- Gen.pick(1, physicalResources)
    yield Task(taskId, time, prTypes.toList)
