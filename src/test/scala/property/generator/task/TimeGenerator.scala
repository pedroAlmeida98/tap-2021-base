package property.generator.task

import org.scalacheck.Gen

import domain.task.Time
import property.generator.SimpleGenerators.genNonNegativeInteger

object TimeGenerator:

  /**
   * Generates the simple type Time
   *
   * @return the Time
   */
  def genTime: Gen[Time] =
    for
      int <- genNonNegativeInteger
      time <- Time.from(int.toString).fold(_ => Gen.fail, res => Gen.const(res))
    yield time